// Fill out your copyright notice in the Description page of Project Settings.


#include "PositionNotifiable.h"
#include "Position.h"

// Add default functionality here for any IPositionNotifiable functions that are not pure virtual.

TArray<FElementPosition> IPositionNotifiable::GetPosition()
{
    return TArray<FElementPosition>();
}
