// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/UserDefinedStruct.h"
#include "Position.generated.h"

/**
 * 
 */
USTRUCT()
struct FElementPosition
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY()
	int32 X;
	UPROPERTY()
	int32 Y;
};
