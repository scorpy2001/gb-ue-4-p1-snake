// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Position.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	ElementSize = 100.0f;

	MovementSpeed = 0.5f;
	LastMovementDirection = EMovementDirection::RIGHT;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	AddSnakeElement(4);
	SetActorTickInterval(MovementSpeed);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		auto NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		NewSnakeElement->SnakeOwner = this;

		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		if(ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector = FVector::ZeroVector;
	float MovementSpeedDelta = ElementSize;
	
	switch (LastMovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeedDelta;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeedDelta;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += MovementSpeedDelta;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= MovementSpeedDelta;
		break;
	default:
		break;
	}

	SnakeElements[0]->ToogleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PreviousElement = SnakeElements[i - 1];

		FVector PreviousElementLocation = PreviousElement->GetActorLocation();
		CurrentElement->SetActorLocation(PreviousElementLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToogleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 SnakeElementIndex;
		SnakeElements.Find(OverlappedElement, SnakeElementIndex);
		bool bIsFirst = (SnakeElementIndex == 0);

		IInteractable* Interactable = Cast<IInteractable>(Other);
		if (Interactable)
		{
			Interactable->Interact(this, bIsFirst);
		}
	}
}

TArray<FElementPosition> ASnakeBase::GetPosition()
{
	Positions.Empty();

	for (auto i = 0; i < SnakeElements.Num(); i++)
	{
		auto SnakeElement = SnakeElements[i];
		auto realPosition = SnakeElement->GetActorLocation();
		FElementPosition position = FElementPosition();
		position.X = realPosition.X / ElementSize;
		position.Y = realPosition.Y / ElementSize;
		Positions.Add(position);
	}

	return Positions;
}

