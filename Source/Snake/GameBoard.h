// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameBoard.generated.h"

class AGameBoardElement;
class AObstacle;
class ASnakeBase;
class AFood;
struct FElementPosition;

UENUM()
enum class EGameBoardElementType
{
	STATIC_OBSTACLE,
	DYNAMIC_OBSTACLE,
	EMPTY,
	FOOD,
	SNAKE
};

UCLASS()
class SNAKE_API AGameBoard : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameBoard();

	UPROPERTY(EditDefaultsOnly)
	int32 GameBoardSizeX = 10;
	UPROPERTY(EditDefaultsOnly)
	int32 GameBoardSizeY = 10;

	UPROPERTY(EditDefaultsOnly)
	float SecondsToRefreshObstacles = 0.5f;

	UPROPERTY(EditDefaultsOnly)
	int32 NumberObstacles = 2;


	UPROPERTY(EditDefaultsOnly)
	float GameBoardZOffsetPosition = -50.0f;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AGameBoardElement> GameBoardElementClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AObstacle> ObstacleClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize = 100.0f;

	TArray<AObstacle*> DynamicObstacles;
	AFood* DynamicFood;
	ASnakeBase* Snake;
	TArray<FElementPosition*> SnakeElementPositios;

	float LastTime = 0.0f;
	int32 xOffset = 0;
	int32 yOffset = 0;

	TArray<EGameBoardElementType> GameBoardElementTypes;
	void ResetDynamicObstacle();
	void ResetSnake();

	void CreateFood();
	void CleanFood();
	TArray<int32> GetGameBoardEmptyElements();
	UFUNCTION()
	void HandleFoodDestroed();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void CreateGameBoard();
	AObstacle* CreateObstacle(float xPosition, float yPosition, float zPosition);
};
