// Fill out your copyright notice in the Description page of Project Settings.

#include "GameBoard.h"
#include "GameBoardElement.h"
#include "Obstacle.h"
#include "SnakeBase.h"
#include "Food.h"

// Sets default values
AGameBoard::AGameBoard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AGameBoard::ResetDynamicObstacle()
{
	for (auto i = 0; i < GameBoardElementTypes.Num(); i++)
	{
		if (GameBoardElementTypes[i] == EGameBoardElementType::DYNAMIC_OBSTACLE)
		{
			GameBoardElementTypes[i] = EGameBoardElementType::EMPTY;
		}
	}

	for (auto i = 0; i < DynamicObstacles.Num(); i++)
	{
		DynamicObstacles[i]->Destroy();
	}

	DynamicObstacles.Empty();

	auto EmptyGameBoardElements = GetGameBoardEmptyElements();

	if (EmptyGameBoardElements.Num() > 0)
	{
		auto ObstaclesToCreate = EmptyGameBoardElements.Num() < NumberObstacles ? EmptyGameBoardElements.Num() : NumberObstacles;
		for (auto i = 0; i < ObstaclesToCreate; i++)
		{
			int32 indexElement = FMath::RandRange(0, EmptyGameBoardElements.Num() - 1);
			int32 EmptyGameBoardElementIndex = EmptyGameBoardElements[indexElement];
			int32 yIndex = EmptyGameBoardElementIndex % GameBoardSizeY;
			int32 xIndex = (EmptyGameBoardElementIndex - yIndex) / GameBoardSizeY;
			auto newObstacle = CreateObstacle((xIndex - xOffset) * ElementSize, (yIndex - yOffset) * ElementSize, -GameBoardZOffsetPosition);
			GameBoardElementTypes[EmptyGameBoardElementIndex] = EGameBoardElementType::DYNAMIC_OBSTACLE;
			DynamicObstacles.Add(newObstacle);
			EmptyGameBoardElements.RemoveAt(indexElement);
		}
	}
}

void AGameBoard::ResetSnake()
{
	for (auto i = 0; i < GameBoardElementTypes.Num(); i++)
	{
		if (GameBoardElementTypes[i] == EGameBoardElementType::SNAKE)
		{
			GameBoardElementTypes[i] = EGameBoardElementType::EMPTY;
		}
	}

	auto SnakeElementPositions = Snake->GetPosition();

	for (auto i = 0; i < SnakeElementPositions.Num(); i++)
	{
		if (SnakeElementPositions[i].Y < GameBoardSizeY && SnakeElementPositions[i].X < GameBoardSizeX)
		{
			auto GameBoardIndex = (SnakeElementPositions[i].X + xOffset) * GameBoardSizeY + SnakeElementPositions[i].Y + yOffset;
			if (GameBoardIndex >= 0 && GameBoardElementTypes.Num() > GameBoardIndex)
			{
				GameBoardElementTypes[GameBoardIndex] = EGameBoardElementType::SNAKE;
			}
		}
	}
}

void AGameBoard::CreateFood()
{
	ResetSnake();

	auto EmptyGameBoardElements = GetGameBoardEmptyElements();

	int32 indexElement = FMath::RandRange(0, EmptyGameBoardElements.Num() - 1);
	int32 EmptyGameBoardElementIndex = EmptyGameBoardElements[indexElement];
	int32 yIndex = EmptyGameBoardElementIndex % GameBoardSizeY;
	int32 xIndex = (EmptyGameBoardElementIndex - yIndex) / GameBoardSizeY;
	DynamicFood = GetWorld()->SpawnActor<AFood>(FoodClass, FTransform(FVector((xIndex - xOffset) * ElementSize, (yIndex - yOffset) * ElementSize, -GameBoardZOffsetPosition)));
	DynamicFood->OnDestroedEvent.AddDynamic(this, &AGameBoard::HandleFoodDestroed);
	CleanFood();
	GameBoardElementTypes[EmptyGameBoardElementIndex] = EGameBoardElementType::FOOD;
}

void AGameBoard::CleanFood()
{
	for (auto i = 0; i < GameBoardElementTypes.Num(); i++)
	{
		if (GameBoardElementTypes[i] == EGameBoardElementType::FOOD)
		{
			GameBoardElementTypes[i] = EGameBoardElementType::EMPTY;
		}
	}
}

TArray<int32> AGameBoard::GetGameBoardEmptyElements()
{
	TArray<int32> EmptyGameBoardElements;
	for (auto i = 0; i < GameBoardElementTypes.Num(); i++)
	{
		if (GameBoardElementTypes[i] == EGameBoardElementType::EMPTY)
		{
			EmptyGameBoardElements.Add(i);
		}
	}

	return EmptyGameBoardElements;
}

void AGameBoard::HandleFoodDestroed()
{
	CreateFood();
}

// Called when the game starts or when spawned
void AGameBoard::BeginPlay()
{
	Super::BeginPlay();

	xOffset = GameBoardSizeX / 2;
	yOffset = GameBoardSizeY / 2;

	AddActorWorldOffset(FVector(0, 0, GameBoardZOffsetPosition));
	CreateGameBoard();
}

// Called every frame
void AGameBoard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	LastTime += DeltaTime;

	if (LastTime >= SecondsToRefreshObstacles)
	{
		ResetSnake();

		ResetDynamicObstacle();

		LastTime = 0.0f;
	}
}

void AGameBoard::CreateGameBoard()
{
	for (int32 i = 0; i < GameBoardSizeX * GameBoardSizeY; i++)
	{
		int32 yIndex = i % GameBoardSizeY;
		int32 xIndex = (i - yIndex) / GameBoardSizeY;
		//int32 yIndex = i / GameBoardSizeY;
		//int32 xIndex = i - (yIndex * GameBoardSizeY);

		FVector NewLocation((xIndex - xOffset) * ElementSize, (yIndex - yOffset) * ElementSize, 0);
		FTransform NewTransform(NewLocation);
		auto NewGameBoardElementElement = GetWorld()->SpawnActor<AGameBoardElement>(GameBoardElementClass, NewTransform);
		//NewGameBoardElementElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		NewGameBoardElementElement->GameBoardOwner = this;

		if (xIndex == 0 || xIndex == GameBoardSizeX - 1 || yIndex == 0 || yIndex == GameBoardSizeY - 1)
		{
			CreateObstacle((xIndex - xOffset) * ElementSize, (yIndex - yOffset) * ElementSize, -GameBoardZOffsetPosition);
			GameBoardElementTypes.Add(EGameBoardElementType::STATIC_OBSTACLE);
		}
		else
		{
			GameBoardElementTypes.Add(EGameBoardElementType::EMPTY);
		}
	}
}

AObstacle* AGameBoard::CreateObstacle(float xPosition, float yPosition, float zPosition)
{
	return GetWorld()->SpawnActor<AObstacle>(ObstacleClass, FTransform(FVector(xPosition, yPosition, zPosition)));
}

